import AllMeetupsPage from "./pages/AllMeetupsPage";
import FavoritesPage from "./pages/Favorites";
import NewMeetupsPage from "./pages/NewMeetup";

import MainNavigation from "./components/layout/MainNavigation";
import Layout from "./components/layout/Layout";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { MeetupProvider } from "./components/meetups/MeetupProvider";

function App() {
  return (
    <div data-test="app">
      <MeetupProvider>
        <MainNavigation />
        <Layout>
          <Router>
            <Routes>
              <Route path="/" element={<AllMeetupsPage />} />
              <Route path="favourites" element={<FavoritesPage />} />
              <Route path="new-meetup" element={<NewMeetupsPage />} />
              <Route
                path="*"
                element={
                  <main style={{ padding: "1rem" }}>
                    <p>There's nothing here!</p>
                  </main>
                }
              />
            </Routes>
          </Router>
        </Layout>
      </MeetupProvider>
    </div>
  );
}

export default App;
