export const URLS = [
  { url: "/", label: "All Meetups" },
  { url: "/new-meetup", label: "Add New Meetup" },
  { url: "/favourites", label: "My Favorites" },
];
