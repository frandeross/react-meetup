import classes from "./List.module.css";

export default function Card({ children }) {
  return <ul className={classes.list}>{children}</ul>;
}
