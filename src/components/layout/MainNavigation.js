import { useEffect, useState } from "react";
import { URLS } from "../../utils/constants";

import classes from "./MainNavigation.module.css";

export default function MainNavigation({ page }) {
  const [show, setShow] = useState(true);
  const [lastScrollY, setLastScrollY] = useState(0);

  const controlNavbar = () => {
    if (typeof window !== "undefined") {
      if (window.scrollY > lastScrollY) {
        setShow(false);
      } else {
        setShow(true);
      }
      setLastScrollY(window.scrollY);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", controlNavbar);

      return () => {
        window.removeEventListener("scroll", controlNavbar);
      };
    }
  }, [lastScrollY]);

  const pathname = window.location.pathname;

  return (
    <header
      className={`${classes.header} ${!show ? classes["header--hidden"] : ""}`}
      data-test="navigation-header"
    >
      <div className={classes.logo}>React Meetups</div>
      <nav>
        <ul>
          {URLS.map((direction, index) => (
            <li key={index}>
              <a
                className={`${classes.logo} ${
                  pathname == direction.url ? classes["link--disabled"] : ""
                }`}
                href={direction.url}
              >
                {direction.label}
              </a>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
}
