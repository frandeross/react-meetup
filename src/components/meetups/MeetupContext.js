import { createContext } from "react";

const meetupsContext = createContext();

export default meetupsContext;
