/* eslint-disable testing-library/no-debugging-utils */
import { shallow } from "enzyme";
import MeetupItem from "./MeetupItem";

const randomTextGenerator = (size = 5) =>
  Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, "")
    .substr(0, size);

describe("<MeetupItem />", () => {
  let wrapper = shallow(<MeetupItem />);

  const handleFavourite = (meetId) => meetId;

  const testData = {
    id: "",
    title: "",
    image: "",
    address: "",
    description: "",
  };

  let favourite;

  beforeEach(() => {
    wrapper = shallow(<MeetupItem />);

    testData.id = randomTextGenerator();
    testData.title = randomTextGenerator(6);
    testData.image = `https://${randomTextGenerator(7)}.jpg`;
    testData.address = randomTextGenerator(14);
    testData.description = randomTextGenerator(15);

    favourite = Math.random() < 0.5;
  });

  it("Should renders without crashing", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("Should show contain the proper values for the h3", () => {
    const wrapper = shallow(<MeetupItem data={testData} />);
    expect(wrapper.find("h3").text()).toEqual(testData.title);
  });

  it("Should show contain the proper values for the address", () => {
    const wrapper = shallow(<MeetupItem data={testData} />);
    expect(wrapper.find("address").text()).toEqual(testData.address);
  });

  it("Should show contain the proper values for the description", () => {
    const wrapper = shallow(<MeetupItem data={testData} />);
    expect(wrapper.find("p").text()).toEqual(testData.description);
  });

  it("Should contain the proper values for the img props", () => {
    const wrapper = shallow(<MeetupItem data={testData} />);
    expect(wrapper.find("img").prop("src")).toEqual(testData.image);
    expect(wrapper.find("img").prop("alt")).toEqual(testData.title);
  });

  it("Should contain the proper values for the button depending on the favourite estatus", () => {
    const wrapper = shallow(
      <MeetupItem data={testData} favourite={favourite} />
    );

    if (favourite) {
      expect(wrapper.find("button").text()).toEqual("Remove from favorites");
    } else {
      expect(wrapper.find("button").text()).toEqual("Add to favorites");
    }
  });

  it("On click the button should return the meet id", () => {
    const wrapper = shallow(
      <MeetupItem
        data={testData}
        favourite={favourite}
        handleFavourite={handleFavourite}
      />
    );
    expect(wrapper.find("button").simulate("click").equals(testData.id));
  });
});
