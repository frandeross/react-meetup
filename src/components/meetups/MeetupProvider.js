import { useMeets } from "../../util-hooks/useMeets";
import MeetupContext from "./MeetupContext";

export const MeetupProvider = (props) => {
  const { meetups, setMeetups, favourites, setFavourites } = useMeets({
    url: "/data.json",
  });

  return (
    <MeetupContext.Provider
      value={{
        meetups: meetups,
        favourites: favourites,
        addNewMeetup: (newMeetupInfo) => {
          console.log(newMeetupInfo);
          const newMeetupsList = [
            { ...newMeetupInfo, id: `m${meetups.length + 1}` },
            ...meetups,
          ];
          setMeetups(newMeetupsList);
        },
        changeFavouriteStatus: (meetupId) => {
          const favouriteIndex = favourites.indexOf(meetupId);
          const newFavouritesList = [...favourites];

          if (favouriteIndex > -1) {
            newFavouritesList.splice(favouriteIndex, 1);
          } else {
            newFavouritesList.push(meetupId);
          }
          console.log(newFavouritesList);

          setFavourites(newFavouritesList);
        },
      }}
    >
      {props.children}
    </MeetupContext.Provider>
  );
};
