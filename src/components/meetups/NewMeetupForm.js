import Card from "../ui/Card";
import classes from "./NewMeetupForm.module.css";
import { useContext } from "react";
import MeetupsContext from "./MeetupContext";
import { useNavigate } from "react-router-dom";

export default function NewMeetupForm() {
  const meetupsUtilities = useContext(MeetupsContext);
  const navigate = useNavigate();

  const submitHandler = (event) => {
    event.preventDefault();

    const newMeetup = {
      title: event.target.title.value,
      image: event.target.image.value,
      address: event.target.address.value,
      description: event.target.description.value,
    };
    meetupsUtilities.addNewMeetup(newMeetup);
    navigate("/");
  };

  return (
    <Card>
      <form className={classes.form} onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor="title">Meetup Title</label>
          <input type="text" required id="title" />
        </div>
        <div className={classes.control}>
          <label htmlFor="image">Meetup Image</label>
          <input type="url" required id="image" />
        </div>
        <div className={classes.control}>
          <label htmlFor="address">Address</label>
          <input type="text" required id="address" />
        </div>
        <div className={classes.control}>
          <label htmlFor="description">Description</label>
          <textarea id="description" required rows="5"></textarea>
        </div>
        <div className={classes.actions}>
          <button type="submit">Add Meetup</button>
        </div>
      </form>
    </Card>
  );
}
