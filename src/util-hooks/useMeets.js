import { useEffect } from "react";
import { useLocalStorage } from "./useLocalStorage";

export const useMeets = (options) => {
  const [meetups, setMeetups] = useLocalStorage("meetups", []);
  const [favourites, setFavourites] = useLocalStorage("favourites", []);

  useEffect(() => {
    if (!meetups.length) {
      fetch(options.url)
        .then((response) => response.json())
        .then((json) => {
          setMeetups(json);
        });
    }
  }, []);

  return {
    meetups,
    setMeetups,
    favourites,
    setFavourites,
  };
};
