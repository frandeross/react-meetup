import List from "../components/ui/List";
import { useContext } from "react";
import MeetupsContext from "../components/meetups/MeetupContext";
import MeetupItem from "../components/meetups/MeetupItem";

const FavoritesPage = () => {
  const meetupsUtilities = useContext(MeetupsContext);

  return (
    <section>
      <h1>Favorites Page</h1>
      <List>
        {meetupsUtilities.meetups &&
          meetupsUtilities.meetups.map((meetup) => {
            const isFav = meetupsUtilities.favourites.indexOf(meetup.id) > -1;
            if (isFav)
              return (
                <MeetupItem
                  key={meetup.id}
                  data={meetup}
                  favourite={true}
                  handleFavourite={meetupsUtilities.changeFavouriteStatus}
                />
              );
          })}
      </List>
    </section>
  );
};

export default FavoritesPage;
