import MeetupItem from "../components/meetups/MeetupItem";
import List from "../components/ui/List";
import { useContext } from "react";
import MeetupsContext from "../components/meetups/MeetupContext";

export default function AllMeetupsPage() {
  const meetupsUtilities = useContext(MeetupsContext);

  return (
    <section>
      <h1>All Meetups</h1>
      <List>
        {meetupsUtilities.meetups &&
          meetupsUtilities.meetups.map((meetup) => {
            const isFav = meetupsUtilities.favourites.indexOf(meetup.id) > -1;
            return (
              <MeetupItem
                key={meetup.id}
                data={meetup}
                favourite={isFav}
                handleFavourite={meetupsUtilities.changeFavouriteStatus}
              />
            );
          })}
      </List>
    </section>
  );
}
